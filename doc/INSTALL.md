___
## INSTALACIÓN
___

# Prerrequisitos

La siguiente instalación y configuración  realizado en Sistema Operativo Debian Jessie

**Creacion de Usuario en Sistema Operativo**

```sh
$ sudo adduser usuarioPersonal
```

**Agregar Usuario al grupo sudo del Sistema Operativo**

```sh
$ sudo adduser usuarioPersonal sudo
```

**Instalando GIT**

Git (pronunciado "guit" ) es un software de control de versiones diseñado por Linus Torvalds, pensando en la eficiencia y la confiabilidad del mantenimiento de versiones de aplicaciones cuando éstas tienen un gran número de archivos de código fuente.

Para la instalación realizar:
```sh
$ sudo apt-get install git
```

**Instalando nodejs y npm**

>Node.js es un entorno de desarrollo basado en JavaScript que permite la ejecución de servicios y eventos del lado del servidor. Existen múltiples librerias que podriamos incorporar a Node,js, esto lo haremos a través de su administrador NPM.

>
**Instalar Curl***

>Durante el proceso de instalación necesitaremos este binario.
```sh
$ sudo apt-get install curl
```

>
**Añadiendo los repositorios de NodeSource***

>Para añadir los repositorios de node.js version 4.x debemos de ejecutar:
```sh
$ sudo curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -
```

>
**Instalando Node.js***

>Ahora realizaremos la instalacion de node v4.4.0
```sh
$ sudo apt-get install -y nodejs
```

>
**Instalando NPM***

>npm(node package manager) es el gestor de paquetes de Node.js, nos permitira descargar librerias y enlazarlas.

>Debemos de seguir de la sigueinte manera:
```sh
$ git clone http://github.com/isaacs/npm.git
$ cd npm
$ sudo make install
```

>Para verificar si esta instalado correctamente:
```sh
$ npm info
```


**Instalando Gruntjs**

Un manejador de tareas, permite minimización, compilación, pruebas unitarias, y mucho más.
```sh
$ sudo npm install -g grunt-cli
```

**Instalando PostgreSQL**

PostgreSQL es un potente sistema de base de datos objeto-relacional de código abierto.

***Instalación***
```sh
$ sudo apt-get install postgresql postgresql-client postgresql-contrib
```

***Configuración***
Para poder realizar operaciones relacionadas con PostgreSQL:
>
```sh
$ sudo su - postgres
```

Creando usuario para el sistema:
```sqldb
postgres=# CREATE USER usuarioPersonal PASSWORD 'password';
```

Cambiando privilegios de usuario:
```sqlDB
postgres=# ALTER ROLE usuarioPersonal WITH SUPERUSER;
```

Creando la Base de Datos para el sistema
```sqldb
postgres=# CREATE DATABASE moduloPersonalDb WITH OWNER usuarioPersonal;
```

## Clonar repositorio

```sh
$ sudo su - usuarioPersonal
$ cd ~
$ git clone git@gitlab.geo.gob.bo:SistemaGestionAdministrativa/ModuloPersonal.git
$ cd ModuloPersonal
```

Para instalar las dependecias del servidor
```sh
$ npm install
```

Para iniciar el servidor de desarrollo en el puerto 9000
```sh
$ grunt serve
```

## Instalacion y configuracion de servidor NGinx

Servidor nginx (pronunciado en inglés “engine X”) es un servidor web/proxy inverso ligero de alto rendimiento, optimizado para aplicaciones Node.js.

A continuación, instalar Nginx
```sh
$ sudo apt-get install nginx
```

Ahora abre el archivo de configuración del bloque servidor por defecto para la edición:
```sh
$ sudo nano /etc/nginx/sites-available/default
```

Insertar la siguiente configuración. Asegúrese de sustituir su propio nombre de dominio para el server (o la dirección IP si usted no tiene un dominio configurado), y la aplicación dirección IP-Privada del servidor para el APP_PRIVATE_IP_ADDRESS . Además, cambiar el puerto ( 9000 ) si la aplicación está configurado para escuchar en un puerto diferente:
```sh
server {
    listen 80;

    server_name sitio-de-prueba.gob.bo;

    location / {
        proxy_pass http://APP_PRIVATE_IP_ADDRESS:9000;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
    }
}
```

Una vez que haya terminado de añadir los bloques de localización para sus aplicaciones, guardar y salir.
En el servidor, reiniciar Nginx:
```sh
$ sudo /et c/init.d/nginx restart
```

Esto configura la web del servidor para responder a las solicitudes en su raíz. Suponiendo que nuestro servidor está disponible en example.com , el acceso a http://sitio-de-prueba.gob.bo/ a través de un navegador web que enviar la solicitud a la dirección IP privada del servidor de aplicaciones en el puerto 9000 , que se recibió y respondió a la Node.js solicitud.
