# modulo-personal

Este es el modulo de personal del sistema de gestion administrativa

## Desarrolladores
- D'jalmar
- David
- Gustavo

### Prerequisitos

- [Git](https://git-scm.com/)
- [Node.js and npm](nodejs.org) Node ^4.2.3, npm ^2.14.7
- [Grunt](http://gruntjs.com/) (`npm install --global grunt-cli`)
- [Postgresql](http://www.postgresql.org.es/)

### Para iniciar el proyecto en desarrollo se deben ejecutar los siguientes comandos en la terminal

1. Ejecutar `npm install` para instalar las dependecias del servidor.

2. Ejecutar `grunt serve` para iniciar el servidor de desarrollo. Debe iniciar automaticamente el explorador.

## Compilacion y desarrollo

Ejecutar `grunt build` para compilar el proyecto, el proyecto compilado se encuentra en la carpeta `dist` luego ejecutar  `grunt serve` para una vista previa.

## Tests

Ejecutar `npm test` para ejecutar todos los test(unitarios y de integracion) del proyecto
